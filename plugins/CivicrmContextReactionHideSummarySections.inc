<?php

/**
 * @file
 * Contains CivicrmContextReactionHideSummarySections.
 */

/**
 * Hide CiviCRM summary sections as a context reaction.
 */
class CivicrmContextReactionHideSummarySections extends context_reaction {
  /**
   * Editor form.
   */
  public function editor_form($context) {
    $form = $this->options_form($context);
    return $form;
  }

  /**
   * Submit handler for editor form.
   */
  public function editor_form_submit($context, $values) {
    return $values;
  }

  /**
   * Allow admins to select which summary sections to hide.
   */
  public function options_form($context) {
    $values = $this->fetch_from_context($context);
    civicrm_initialize();
    $options = $this->getAllSections();
    $default = isset($values['civicrm_hide_sections_list']) ?
      $values['civicrm_hide_sections_list'] : array();
    $form = array(
      '#tree' => TRUE,
      '#title' => t('Hide Summary Sections'),
      'civicrm_hide_sections_list' => array(
        '#title' => t('Sections'),
        '#description' => t('Hides these sections on CiviCRM contact pages when this context is active.'),
        '#type' => 'select',
        '#options' => $options,
        '#multiple' => TRUE,
        '#size' => (count($options) > 12) ? 12 : count($options),
        '#default_value' => $default,
      ),
    );
    return $form;
  }

  /**
   * Hide summary sections needed.
   *
   * @param object $page
   *  The page object passed from hook_civicrm_pageRun().
   */
  public function execute(&$page) {
    $vars = $page->get_template_vars();
    $viewCustomData = array();
    $changed = FALSE;
    if (isset($vars['viewCustomData'])) {
      $viewCustomData = $vars['viewCustomData'];
    }

    foreach ($this->get_contexts() as $context) {
      $hide_sections = $context->reactions[$this->plugin]['civicrm_hide_sections_list'];

      // Each entry in $hide_sections is in the form 'showXX' or the form 'custom_YY'.
      foreach ($hide_sections as $k => $var) {
        if (substr($k, 0, 4) == 'show') {
          if (in_array($vars, $k) !== FALSE) {
            $page->assign($k, '0');
          }
        }
        elseif (substr($k, 0, 7) == 'custom_') {
          $custom_id = str_replace('custom_', '', $k);
          if (isset($viewCustomData[$custom_id])) {
            unset($viewCustomData[$custom_id]);
            $changed = TRUE;
          }
        }
      }
      if ($changed) {
        $page->assign('viewCustomData', $viewCustomData);
      }
    }
  }

  /**
   * Get list of CiviCRM summary sections for options form.
   *
   * For now this only provides the options from the settings available in
   * 'contact_edit_options'.
   *
   * @return array
   *   A list of sections available to hide.
   */
  private function getAllSections() {
    $sections = array();
    //Get blocks and if they are visible in edit form.
    $editOptions = CRM_Core_BAO_Setting::valueOptions(
      CRM_Core_BAO_Setting::SYSTEM_PREFERENCES_NAME, 'contact_edit_options');
    foreach ($editOptions as $blockName => $value) {
      $varName = 'show' . $blockName;
      if ($value) {
        $sections[$varName] = t('@bn (Active)', array('@bn' => $blockName)) ;
      }
      else {
        $sections[$varName] = t('@bn (Inactive)', array('@bn' => $blockName)) ;
      }
    }
    $this->getCustomGroupSections($sections);
    return $sections;
  }

  /**
   * Get sections for custom groups.
   *
   * @param array $all_sections
   *   List of tabs so far.
   */
  private function getCustomGroupSections(array &$all_sections) {
    // Now add all the custom groups using Inline style - even inactive ones.
    $result = civicrm_api3('CustomGroup', 'get', array(
     'style' => 'Inline',
    ));
    if ($result['is_error'] == '0' && $result['count'] >= 1) {
      foreach ($result['values'] as $id => $group) {
        $all_sections["custom_{$id}"] = check_plain($group['title']);
      }
    }
  }

}
