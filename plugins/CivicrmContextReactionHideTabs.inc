<?php

/**
 * @file
 * Contains CivicrmContextReactionHideTabs.
 */

/**
 * Hide CiviCRM summary as context reactions.
 */
class CivicrmContextReactionHideTabs extends context_reaction {
  /**
   * Editor form.
   */
  public function editor_form($context) {
    $form = $this->options_form($context);
    return $form;
  }

  /**
   * Submit handler for editor form.
   */
  public function editor_form_submit($context, $values) {
    return $values;
  }

  /**
   * Allow admins to select which tabs to hide.
   */
  public function options_form($context) {
    $values = $this->fetch_from_context($context);
    civicrm_initialize();
    $options = $this->getAllTabs();
    $form = array(
      '#tree' => TRUE,
      '#title' => t('Hide Tabs'),
      'civicrm_hide_tabs_list' => array(
        '#title' => t('Tabs'),
        '#description' => t('Hides these tabs on CiviCRM contact pages when this context is active.'),
        '#type' => 'select',
        '#options' => $options,
        '#multiple' => TRUE,
        '#size' => count($options),
        '#default_value' => isset($values['civicrm_hide_tabs_list']) ? $values['civicrm_hide_tabs_list'] : array(),
      ),
    );
    return $form;
  }

  /**
   * Hide tabs if needed.
   *
   * @param array $tabs
   *  The tabs array passed from hook_civicrm_tabs().
   * @param int $contact_id
   *  The contact id passed from hook_civicrm_tabs() (not used).
   */
  public function execute(array &$tabs, $contact_id) {
    foreach ($this->get_contexts() as $context) {
      foreach ($tabs as $key => $tab) {
        $hide_tabs = $context->reactions[$this->plugin]['civicrm_hide_tabs_list'];
        if (!empty($hide_tabs) && is_array($hide_tabs)) {
          foreach ($hide_tabs as $k => $t) {
            if (isset($tab['id']) && $tab['id'] == $k) {
              unset($tabs[$key]);
            }
          }
        }
      }
    }
  }

  /**
   * Get list of CiviCRM tabs for options form.
   *
   * @return array
   *   A list of tabs available to hide.
   */
  private function getAllTabs() {
    $all_tabs = array(
      'activity' => ts('Activities'),
      'rel' => ts('Relationships'),
      'group' => ts('Groups'),
      'note' => ts('Notes'),
      'tag' => ts('Tags'),
      'log' => ts('Change Log'),
    );

    // TODO: If later there is a reaction to show tabs then show the tabs.
    // Only if user has generic access to CiviCRM.
    $components = CRM_Core_Component::getEnabledComponents();

    foreach ($components as $component) {
      $elem = $component->registerTab();
      if (!empty($elem)) {
        // Allow explicit id, if not defined, use keyword instead.
        if (array_key_exists('id', $elem)) {
          $i = $elem['id'];
        }
        else {
          $i = $component->getKeyword();
        }
        $all_tabs[$i] = check_plain($elem['title']);
      }
    }
    $this->getGroupTabs($all_tabs);
    // TODO: see if any other modules want to add any tabs.
    // CRM_Utils_Hook::tabs($all_tabs, $this->_contactId);
    ksort($all_tabs);
    return $all_tabs;
  }

  /**
   * Get tabs for custom groups.
   *
   * @param array $all_tabs
   *   List of tabs so far.
   */
  private function getGroupTabs(array &$all_tabs) {
    // Now add all the custom tabs.
    $result = civicrm_api3('CustomGroup', 'get', array(
      'style' => 'Tab',
    ));
    if ($result['is_error'] == '0' && $result['count'] >= 1) {
      foreach ($result['values'] as $id => $group) {
        $all_tabs["custom_{$id}"] = check_plain($group['title']);
      }
    }
  }

}
