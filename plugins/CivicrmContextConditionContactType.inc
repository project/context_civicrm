<?php

/**
 * @file
 * Contains CivicrmContextConditionContactType.
 */

/**
 * Trigger context on contact view only.
 */
define('CONTEXT_CIVICRM_CONTACT_TYPE_VIEW', 0);

/**
 * Trigger context on contact view and contact edit form.
 */
define('CONTEXT_CIVICRM_CONTACT_TYPE_FORM', 1);

/**
 * Trigger context on contact edit form only.
 */
define('CONTEXT_CIVICRM_CONTACT_TYPE_FORM_ONLY', 2);


/**
 * Expose selected contact types as a context condition.
 */
class CivicrmContextConditionContactType extends context_condition {
  /**
   * The overridden condition_values method.
   */
  public function condition_values() {
    $values = array();
    civicrm_initialize();
    $api_result = civicrm_api3('ContactType', 'get', array(
      'sequential' => 1,
    ));
    if ($api_result['is_error'] == 0) {
      foreach ($api_result['values'] as $value) {
        $values[$value['name']] = check_plain($value['label']);
      }
    }
    return $values;
  }

  /**
   * Allows admins to choose to set context on edit form or not.
   */
  public function options_form($context) {
    $defaults = $this->fetch_from_context($context, 'options');
    $default_value = isset($defaults['civicrm_contact_type_edit_form']) ?
      $defaults['civicrm_contact_type_edit_form'] :
      CONTEXT_CIVICRM_CONTACT_TYPE_VIEW;
    $options = array(
      CONTEXT_CIVICRM_CONTACT_TYPE_VIEW => t('No'),
      CONTEXT_CIVICRM_CONTACT_TYPE_FORM => t('Yes'),
      CONTEXT_CIVICRM_CONTACT_TYPE_FORM_ONLY => t('Only on edit form'),
    );
    return array(
      'civicrm_contact_type_edit_form' => array(
        '#title' => t('Set on contact edit form'),
        '#type' => 'select',
        '#options' => $options,
        '#description' => t('Set this context on contact edit forms'),
        '#default_value' => $default_value,
      ),
    );
  }

  /**
   * The overridden execute method.
   */
  public function execute($contact, $op) {
    // Get combined list of contexts for 'contact_type' and 'contact_sub_type'.
    $base_contexts = $this->get_contexts($contact['contact_type']);
    $sub_contexts = array();
    if (isset($contact['contact_sub_type']) && !empty($contact['contact_sub_type'])) {
      if (!is_array($contact['contact_sub_type'])) {
        $contact['contact_sub_type'] = array($contact['contact_sub_type']);
      }
      foreach ($contact['contact_sub_type'] as $sub) {
        $sub_contexts[$sub] = $this->get_contexts($sub);
      }
    }

    foreach ($base_contexts as $context) {
      $this->executeContext($context, $op, $contact['contact_type']);
    }
    foreach ($sub_contexts as $sub => $contexts) {
      foreach ($contexts as $context) {
        $this->executeContext($context, $op, $sub);
      }
    }
  }

  /**
   * Generalize the condition_met code.
   *
   * @param object $context
   *   The context to execute.
   * @param string $op
   *   The operation, edit or view.
   * @param string $contact_type_key
   *   The condition value.
   */
  protected function executeContext($context, $op, $contact_type_key) {
    // Check the contact edit form option.
    $options = $this->fetch_from_context($context, 'options');
    if ($op === 'edit') {
      if (!empty($options['civicrm_contact_type_edit_form']) &&
        in_array($options['civicrm_contact_type_edit_form'],
          array(CONTEXT_CIVICRM_CONTACT_TYPE_FORM, CONTEXT_CIVICRM_CONTACT_TYPE_FORM_ONLY))
      ) {
        $this->condition_met($context, $contact_type_key);
      }
    }
    elseif (empty($options['civicrm_contact_type_edit_form']) ||
      $options['civicrm_contact_type_edit_form'] != CONTEXT_CIVICRM_CONTACT_TYPE_FORM_ONLY
    ) {
      $this->condition_met($context, $contact_type_key);
    }
  }

}
